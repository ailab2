\documentclass[a4paper, 12pt]{article}
\usepackage[swedish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{parskip}
% Include pdf with multiple pages ex \includepdf[pages=-, nup=2x2]{filename.pdf}
\usepackage[final]{pdfpages}
% Place figures where they should be
\usepackage{float}

% vars
\def\title{Sökning}
\def\preTitle{Laboration 2}
\def\kurs{Artificiell Intelligens med inriktning mot kognition och
  design B, ht 2008}

\def\namn{Anton Johansson}
\def\mail{dit06ajn@cs.umu.se}
\def\namnett{Victor Zamanian}
\def\mailett{dit06vzy@cs.umu.se}

\def\handledareEtt{Dennis Olsson, denniso@cs.umu.se}
\def\inst{datavetenskap}
\def\dokumentTyp{Laborationsrapport}

\begin{document}
\begin{titlepage}
  \thispagestyle{empty}
  \begin{small}
    \begin{tabular}{@{}p{\textwidth}@{}}
      UMEÅ UNIVERSITET \hfill \today \\
      Institutionen för \inst \\
      \dokumentTyp \\
    \end{tabular}
  \end{small}
  \vspace{10mm}
  \begin{center}
    \LARGE{\preTitle} \\
    \huge{\textbf{\kurs}} \\
    \vspace{10mm}
    \LARGE{\title} \\
    \vspace{15mm}
    \begin{large}
      \namn, \mail \\
      \namnett, \mailett
    \end{large}
    \vfill
    \large{\textbf{Handledare}}\\
    \mbox{\large{\handledareEtt}}
  \end{center}
\end{titlepage}

\pagestyle{fancy}
\rhead{\footnotesize{\today}}
\lhead{\footnotesize{\namn, \mail \\ \namnett, \mailett}}
\chead{}
\lfoot{}
\cfoot{}
\rfoot{}

\tableofcontents
\newpage

\rfoot{\thepage}
\pagenumbering{arabic}

\section{Problemspecifikation}
Denna rapport redogör för hur en karta och olika sökalgoritmer
implementeras i programmeringsspråket Java. Huvudfokus i rapporten
ligger på analys och jämförelse mellan de olika sökalgoritmerna.

Fyra algoritmer av två olika typer är implementerade. Bredden-först
sökning och Djupet-först-sökning är två oinformerade sökningar som
söker efter ett givit mål utan någon problemspecifik
kunskap. Greedy-Search och A* är två informerade sökningar som
använder sig av problemspecifik kunskap för att förbättra sin
sökning. I det här fallet används till exempel kortaste vägen från
varje nod till målet för att avgöra sökvägar i kartan.

Labbspecifikation finns att läsa på:\\
\verb!http://www.cs.umu.se/kurser/5DV063/HT08/lab2.html!

\section{Användarhandledning}
Programmet körs från dit06vzy/edu/ai/lab2 med följande kommando:

\begin{footnotesize}
\verb!java -cp lib/jdom.jar:bin/prod SearchInterface MySearcher \! \\
\verb!<sökväg till karta> <metod> [startnod [slutnod]]!
\end{footnotesize}

Där \verb!<sökväg till karta>! ska hänvisa till en karta sparad i
xml-format enligt följande utseende:

\begin{footnotesize}
\begin{verbatim}
<?xml version='1.0' encoding='ISO-8859-1' ?>
<map>
	<node id="Teg" x="1720099" y="7072732">
		<road to="Rondellen" speed="50" />
		<road to="Tegsbron" speed="110" />
	</node>
   ...
</map>
\end{verbatim}
\end{footnotesize}

Metoden som ska angivas, \verb!<metod>!, är \verb!B! för bredden
först, \verb!D! för djupet först, \verb!Ad! för $A^*$ kortaste vägen och
\verb!At! för $A^*$ snabbaste vägen.

Om varken startnod eller slutnod anges efterfrågas de av
programmet. Om en (1) nod anges som parameter antas den vara startnod
och då efterfrågas bara slutnod av programmet. Annars, om både
startnod och slutnod anges körs vald metod (algoritm) för att hitta
vägen mellan noderna.

\section{Algoritmbeskrivning}

\subsection{setMap(File mapFile)}
Denna metod läser in en karta i form av en XML-fil och representerar
den i en graf. Implementationen av grafen är en modifierad version av
Graph.java som vi använde i kursen Datastrukturer och Algoritmer.

När XML-filen lästs in och tolkats som ett DOM-dokument skaffas alla
noder fram ur detta och sparas i grafen som objekt av typen
\verb!GraphNode!. Dessa objekt innehåller x- och y-koordinater för
noden och ett ''ID'' vilket är namnet på platsen på kartan som noden
representerar. Sedan gås alla noder igenom ännu en gång för att lägga
till vägar och hastighetsbegränsningar mellan dem. Metoden skriver
även ut felmeddelanden om filen 1) inte hittas, 2) inte kan läsas av
någon annan anledning, 3) om det är något fel på innehållet i filen
och vilket felet är, och 4) om någon av koordinaterna för någon nod
inte kan tolkas som ett heltal. Programmet avslutas om något av
ovanstående fel uppstår.

\subsection{Graph.java}
Klassen Graph används för att abstrahera och spara undan alla noder i
kartan. Klassen innehåller en Hashtabell där Kartans namn på noder
sparas som nyckel och själva noden av typ GraphNode sparas som
värde. Metoderna som används mest i denna implementation är de för att
sätta in noder och kanter mellan noderna.

\subsection{GraphNode.java}
Klassen GraphNode används för att spara undan städerna ur
kartan. GraphNode har en Hashtabell som sparar länkar till sina
grannar som nycklar och vikten som värde. Detta innebär att klassen
kan fråga efter en granne och returnera vikten till denna granne.

Dessutom innehåller GraphNode attribut för att spara information om
den är besökt (\textbf{visited}), information om vilken nod den har
besökts ifrån (\textbf{parent}), information om avstånd/tid som det
har kostat för algoritmer att ta sig till aktuell nod
(\textbf{distanceTraveled}) och ett evalueringsvärde 
(\textbf{evalFuncVal}) som noderna kan sorteras efter i en
prioritetskö.

\subsection{Road.java}
Klassen Road används som kanter i grafen. Den sparar undan information
om varje kant/väg i kartan. Detta görs i attributen för hastighet
\textbf{speed}, avstånd \textbf{distance} och tid det tar att färdas
hela vägens sträcka i maxhastighet \textbf{travelTime}.

\subsection{Oinformerade sökalgoritmer}
Följande sökalgoritmer tar emot två parametrar, \textit{from} och
\textit{to} som anger från vilken nod i grafen algoritmen ska börja
och till vilken nod som ska sökas.

Förutom att algoritmerna använder olika typer av datastrukturer för
att lagra noder så agerar de enligt följande procedur:
\begin{enumerate}
\item Lägg till första noden i en \textit{datastruktur}.
\item Ta ut en nod \textbf{n} ur \textit{datastrukturen}.\label{Otaut}
  \begin{enumerate}
  \item Markera noden som besökt.
  \item Om noden som besöks är målnoden avslutas sökningen och
    resultat returneras.
  \item Annars läggs alla obesökta grannoder till i
    \textit{datastrukturen}, om de inte redan finns representerade i
    den, och en länk till noden \textbf{n} sparas i ett attribut
    \textbf{parent}.\label{Oparent}
  \end{enumerate}
\item Börja om från steg \ref{Otaut}.
\end{enumerate}

För varje besökt nod läggs dess namn till i en sträng. Anledningen
till att man sparar föregående besökt nod i ett attribut
\textbf{parent}, i steg \ref{Oparent}, är att när målet har nåtts blir
det möjligt att stega tillbaka med hjälp av dessa attribut för att
hitta vägen algoritmen valt mellan start och mål.

Har algoritmen hittat målnoden returneras en sträng som innehåller alla
besökta noder och en sträng innehållande den hittade vägen från
startnod till målnod.

Är \textit{datastrukturen} i steg \ref{Otaut} tom, så är alla noder
besökta utan att något mål är hittat. En sträng som förklarar det
inträffade returneras.

\subsubsection{breadthFirst(String from, String to)}
Bredden-först använder datastrukturen stack för att spara noder. Detta
medför att den först besökta nodens alla barn kommer att besökas innan
algoritmen går vidare nästa steg djupare i kartan, därav namnet
Bredden-först.

\subsubsection{depthFirst(String from, String to)}
Djupet-först-algoritmen sparar noder i datastrukturen kö. Detta medför
att noderna kommer att plockas ut enligt ''Första in första
ut''-principen, således vandrar algoritmen på djupet i grafen innan
den går på bredden.

\subsection{Informerade sökalgoritmer}
Följande algoritmer använder sig av någon typ av evalueringsfunktion
$f$ som används för bedömning av vilket väg som är bäst att ta.
Evalueringsfunktionen i sin tur beror av någon typ av heuristik,
dvs. någon nivå av kunskap eller information om världen den evaluerar.

\subsubsection{greedySearch(String from, String to)}
Metoden \textit{greedySearch} utför en sökning efter en väg mellan
platserna \verb!from! och \verb!to! efter en ''girig'' algoritm.
Algoritmen börjar på den plats som \verb!from! representerar och
undersöker vilka noder den kan gå till i nästa steg. För var och en av
dessa noder undersöker den vilket steg som leder till att den färdas
geografiskt närmare målnoden (fågelvägen) \verb!to! och väljer detta.

\subsubsection{aStar(String from, String to, boolean fastest)}
Algoritmen $A^*$ (uttalas ''A-sjärna'') fungerar ungefär som
\textit{greedySearch} men den använder ett annat sätt att välja vägen
som den ska ta. Om parametern \verb!fastest! har värdet \verb!true!
söker algoritmen efter den snabbaste vägen (som namnet antyder),
annars söker den efter den kortaste vägen. Funktionen som bestämmer
vilken väg som ska tas i kartan beror på två saker: 1) hur långt den
nod man undersöker ligger från målnoden och 2a) hur långt man färdats
totalt från startnoden eller 2b) hur lång tid det tagit att färdats
från startnoden.

\subsection{API, dokumentation}
En API-dokumentation av implementationen finns på sidan:\\
\verb!http://www.cs.umu.se/~dit06vzy/ai/lab2/doc/!

\section{Begränsningar}
% Vilka problem och begränsningar har vår lösning
Implementationen av grafen (\textit{Graph.java}) använder sig av s.k.
generics, men \verb!Object! valdes som datatyp i några fall.
Anledningen till detta är att vi ville hålla implementationen mer
generell. Detta innebär däremot att vissa datatyper, till exempel
\verb!Road! (\textit{Road.java}) som vi använder för att spara vägar i
grafen, måste explicit konverteras till rätt datatyp innan dess
metoder kan anropas.

\section{Reflektioner}
% Vad var krångligt, hur löste vi det. Allmänna synpunkter.
När målet väl hittats i algoritmerna var det inte helt självklart hur
vägen till målet skulle sparas. Vi löste det genom att algoritmen
''lägger spår'' efter sig när den söker, genom metoden
\verb!GraphNode.setParent(GraphNode node)! som sätter en nods
''förälder'' till \verb!node!. När målnoden nåtts stegar sedan
algoritmerna längs länken som skapats från startnoden till målnoden
och tar fram vägen till målet på så sätt.

Vi hade lite problem med att inse att vi behövde ha en separat
evaluerings\-funktion i A*-algoritmen och ett attribut i klassen
\verb!GraphNode! för att hålla reda på varje nods
evaluerings\-funktions\-värde. Innan vi insåg detta använde vi
attributet i \verb!GraphNode! som egentligen ska hålla reda på
stigkostnad. Vi insåg att det var något fel med vår algoritm då vi
jämförde våra algoritmers lösningar (valda sökvägar) med andra grupper
i klassen.

Metoden setMap i klassen MySearcher måste loopa igenom alla noder ur
xml-filen två gånger. Detta beror på att det inte går att lägga till
en väg mellan en nod och en nod som inte ännu existerar i
minnet. Detta skulle kanske kunnat lösas genom att man faktiskt skapar
den nod som inte finns och lägger till information om nodens position
när man kommer till de noderna senare när man kommer till den noden i
DOM-dokumentet. Metoder hade då behövt skrivas för att sätta
koordinaternas värden för en nod. Vi valde att gå igenom noderna två
gånger i stället därför att det kändes lättast då vi skrev metoden.

Det hade varit bra med klargörande i labbspecifikationen vi fick ut
att kartan kunde ha enkelriktade vägar. Det enda sättet detta gick att
notera var om man gick igenom xml-filen noggrannt och kollade upp
varje väg som fanns. Mycket onödigt felsökande och i vårt fall
omimplementation av hela vår graf hade kunnat undvikas.

\section{Diskussion}
% Redogör för sökalgoritmernas för- och nackdelar, samt hur de
% presterade på den givna uppgiften

\subsection{Bredden-först-sökning}
Fördelen med Bredden-först-sökning var att den är väldigt enkel att
implementera. Den bakomliggande logiken är lätt att förstå och det
behöves ingen specialkunskap om domänen sökningen sker i.

Nackdelen med denna sökning är att om sökningen sker i en stor graf så
kommer algoritmen att besöka väldigt många noder innan den hittar fram
till målet. För varje steg djupare ($d$) algoritmen tar, så ökar
antalet noder som besöks med $d^{b}$, där $b$ = maximala antalet
grannoder, för lite större $d$ kommer detta att bli väldigt många
noder, vilket resulterar i att minnesanvändandet i algoritmen blir
stor.

Bredden-först-algoritmen är komplett, det vill säga den hittar alltid
fram till målet om det finns. Detta går att härleda eftersom
Bredden-först-algoritmen eventuellt kommer att besöka varenda nod som
existerar i grafen.

I en graf där alla vägar väger lika mycket, kommer Bredden-först att
hitta den optimala lösningen (snabbaste/kortaste vägen till mål),
eftersom algoritmen har gått vägen med minst antal steg. För generella
grafer där vägarna kan ha olika viktkostnader kommer dock
Bredden-först inte att vara optimal. Detta är fallet i grafen som
används i denna laboration.

\subsection{Djupet-först-sökning}
Djupet-först-sökning är, precis som Bredden-först-sökning enkel att
implementera, ingen kunskap om målet behövs förutom en koll för att se
när målet är nått.

Liksom Bredden-först-sökning så kommer Djupet-först att besöka alla
noder i grafen tills den har hittat målnoden, algoritmen är således
komplett. Utrymmet som krävs kommer att vara beroende av den djupaste
vägen till målet, eller mer exakt $b^{d}$, där $b$ är bredden eller
antalet grannoder och $d$ är djupet.

I testkörningarna, på sida \pageref{tests}, syns att jämfört med
Bredden-först kommer Djupet-först-sökning att hitta målet utan att
expandera lika många noder. Vägen till målet är dock ofta en lång
omväg eftersom algoritmen, i grafer, ofta kommer att hitta målet på
första djupsökning. Algoritmen är således inte optimal.

Eftersom Djupet-först i denna implementation avnänder sig av ett
attribut, \textbf{visited}, så kommer inte samma nod att besökas två
gånger. Detta gör att algoritmen undviker att hamna i cykler där den
snurrar runt mellan samma noder.

\subsection{Girig sökning}
Fördelen med en girig sökning är att den har någon typ av
evalueringsfunktion $f(n)$ ($f(n) = h(n)$) som gör att den inte
accepterar vilken lösning på ett problem (sökning i karta till
exempel) som helst, utan väljer, om något naivt, en i genomsnitt
bättre lösning än en oinformerad sökning. Nackdelen med en girig
sökning är att den inte tar hänsyn till totala kostnaden för en
lösning jämfört med en annan lösning, utan nöjer sig med första bästa
lösning (s.k. Best-first search).

En annan nackdel med girig sökning är att man skulle kunna hamna i ett
läge där man hoppar mellan två noder i kartan i all oändlighet, givet
att algoritmen inte håller reda på vilka punkter den redan besökt
(vilket vår implementation gör).

\subsection{A* sökning}
En uppenbar fördel med A*-algoritmen är faktumet att den är optimal,
dvs. den hittar den absolut bästa vägen. Den använder sig, som den
giriga sök\-algoritmen, av en evaluerings\-funktion $f(n)$. Men $f(n)$
för A* är mer informerad än den giriga då A* är optimal och den giriga
inte är det. Den ytterligare informationen ligger i att A* håller reda
på hur långt den har gått totalt, alltså stigkostnaden. En nackdel kan
däremot vara att A* måste expandera fler noder för att kunna avgöra
vilken väg som är faktiskt är bäst, och att den då kan kräva lite mer
minne. Något som också tar lite extra minne är att vi sparar värdet
för evaluerings\-funktionen i varje nod. Att den måste expandera fler
noder kan möjligtvis också ta längre tid än mindre informerade (och
icke\-optimala) algoritmer.

\section{Testkörningar}\label{tests}
\subsection{Test Bredden-först}
\subsubsection{From: Tegsbron to Nydala}

\textbf{All expanded nodes:}
Tegsbron, I20, Teg, Station, Obs, Rondellen, Gamlia, Foa, Begbilar, TreKorvar, Kyrkan, NUS, Berghem, Mariehem, Flygplatsen, On, Sofiehem, MIT, Nydala


\textbf{Path to goal:}
Tegsbron, I20, Obs, Foa, Mariehem, Nydala

\subsubsection{From: Flygplatsen to Begbilar}

\textbf{All expanded nodes:}
Flygplatsen, TreKorvar, Rondellen, On, Teg, Kyrkan, Tegsbron, NUS, Station, I20, Gamlia, Sofiehem, Obs, Berghem, GimonAs, Alidhem, Foa, Begbilar


\textbf{Path to goal:}
Flygplatsen, TreKorvar, Rondellen, Kyrkan, Station, Obs, Begbilar

\subsubsection{From: MIT to Flygplatsen}

\textbf{All expanded nodes:}
MIT, Nydala, Berghem, Alidhem, Mariehem, Ok, Gamlia, Sofiehem, Foa, Station, NUS, GimonAs, Begbilar, Obs, I20, Kyrkan, Tegsbron, Rondellen, Teg, TreKorvar, Flygplatsen


\textbf{Path to goal:}
MIT, Berghem, Gamlia, NUS, Kyrkan, Rondellen, TreKorvar, Flygplatsen

\subsubsection{From: Nydala to Gamlia}

\textbf{All expanded nodes:}
Nydala, Ok, Mariehem, MIT, Alidhem, Foa, Berghem, Sofiehem, Obs, Begbilar, Gamlia


\textbf{Path to goal:}
Nydala, Mariehem, Berghem, Gamlia

\subsubsection{From: Teg to Foa}

\textbf{All expanded nodes:}
Teg, Rondellen, Tegsbron, Kyrkan, TreKorvar, I20, Station, NUS, Flygplatsen, On, Obs, Gamlia, Sofiehem, Foa


\textbf{Path to goal:}
Teg, Tegsbron, I20, Obs, Foa

\subsection{Test Djupet-först}
\subsubsection{From: Tegsbron to Nydala}

\textbf{All expanded nodes:}
Tegsbron, Teg, Rondellen, Kyrkan, NUS, Sofiehem, Alidhem, Ok, Nydala


\textbf{Path to goal:}
Tegsbron, Teg, Rondellen, Kyrkan, NUS, Sofiehem, Alidhem, Ok, Nydala

\subsubsection{From: Flygplatsen to Begbilar}

\textbf{All expanded nodes:}
Flygplatsen, TreKorvar, Rondellen, Teg, Tegsbron, I20, Obs, Begbilar


\textbf{Path to goal:}
Flygplatsen, TreKorvar, Rondellen, Teg, Tegsbron, I20, Obs, Begbilar

\subsubsection{From: MIT to Flygplatsen}

\textbf{All expanded nodes:}
MIT, Alidhem, Ok, Sofiehem, GimonAs, NUS, Gamlia, Station, Obs, Begbilar, Foa, Mariehem, I20, Tegsbron, Teg, Rondellen, TreKorvar, On, Flygplatsen


\textbf{Path to goal:}
MIT, Alidhem, Sofiehem, NUS, Gamlia, Station, I20, Tegsbron, Teg, Rondellen, TreKorvar, Flygplatsen

\subsubsection{From: Nydala to Gamlia}

\textbf{All expanded nodes:}
Nydala, MIT, Alidhem, Sofiehem, NUS, Kyrkan, Station, Obs, Begbilar, Foa, I20, Tegsbron, Teg, Rondellen, TreKorvar, Flygplatsen, On, Gamlia


\textbf{Path to goal:}
Nydala, MIT, Alidhem, Sofiehem, NUS, Gamlia

\subsubsection{From: Teg to Foa}

\textbf{All expanded nodes:}
Teg, Tegsbron, I20, Obs, Foa


\textbf{Path to goal:}
Teg, Tegsbron, I20, Obs, Foa

\subsection{Test A* snabbast}
\subsubsection{From: Tegsbron to Nydala}

\textbf{All expanded nodes:}
Tegsbron, Teg, I20, Rondellen, Station, Kyrkan, NUS, TreKorvar, Gamlia, Gamlia, Sofiehem, Flygplatsen, Berghem, Berghem, Alidhem, On, GimonAs, MIT, MIT, MIT, Obs, Ok, Obs, Ok, Mariehem, Mariehem, Nydala


\textbf{Path to goal:}
Tegsbron, Teg, Rondellen, Kyrkan, NUS, Gamlia, Berghem, Mariehem, Nydala

\subsubsection{From: Flygplatsen to Begbilar}

\textbf{All expanded nodes:}
Flygplatsen, TreKorvar, On, Rondellen, Kyrkan, Station, NUS, Gamlia, Teg, Tegsbron, Berghem, I20, I20, Mariehem, Obs, Obs, Gamlia, Sofiehem, Obs, Alidhem, MIT, Foa, Foa, Foa, Foa, MIT, Begbilar


\textbf{Path to goal:}
Flygplatsen, TreKorvar, Rondellen, Teg, Tegsbron, I20, Obs, Foa, Begbilar

\subsubsection{From: MIT to Flygplatsen}

\textbf{All expanded nodes:}
MIT, Alidhem, Sofiehem, Berghem, Gamlia, Nydala, NUS, NUS, GimonAs, Ok, Kyrkan, Kyrkan, Mariehem, Mariehem, Rondellen, Rondellen, Ok, Ok, Station, Station, Station, TreKorvar, TreKorvar, Teg, Teg, Flygplatsen


\textbf{Path to goal:}
MIT, Berghem, Gamlia, NUS, Kyrkan, Rondellen, TreKorvar, Flygplatsen

\subsubsection{From: Nydala to Gamlia}

\textbf{All expanded nodes:}
Nydala, Ok, MIT, Alidhem, Mariehem, Alidhem, Berghem, Berghem, Gamlia


\textbf{Path to goal:}
Nydala, Mariehem, Berghem, Gamlia

\subsubsection{From: Teg to Foa}

\textbf{All expanded nodes:}
Teg, Tegsbron, Rondellen, Kyrkan, I20, Station, Obs, Station, NUS, TreKorvar, Gamlia, Gamlia, Berghem, Berghem, Sofiehem, Flygplatsen, Mariehem, Mariehem, Alidhem, On, Obs, Foa


\textbf{Path to goal:}
Teg, Tegsbron, I20, Station, Obs, Foa

\subsection{Test A* kortast}
\subsubsection{From: Tegsbron to Nydala}

\textbf{All expanded nodes:}
Tegsbron, I20, Station, Gamlia, Berghem, NUS, MIT, Nydala


\textbf{Path to goal:}
Tegsbron, I20, Station, Gamlia, Berghem, MIT, Nydala

\subsubsection{From: Flygplatsen to Begbilar}

\textbf{All expanded nodes:}
Flygplatsen, TreKorvar, On, Rondellen, Kyrkan, Station, NUS, Gamlia, Gamlia, Berghem, Berghem, Obs, Begbilar


\textbf{Path to goal:}
Flygplatsen, TreKorvar, Rondellen, Kyrkan, Station, Obs, Begbilar

\subsubsection{From: MIT to Flygplatsen}

\textbf{All expanded nodes:}
MIT, Alidhem, Sofiehem, Berghem, Gamlia, NUS, NUS, GimonAs, Nydala, Station, Ok, Kyrkan, Ok, Rondellen, Ok, TreKorvar, Flygplatsen


\textbf{Path to goal:}
MIT, Berghem, Gamlia, NUS, Kyrkan, Rondellen, TreKorvar, Flygplatsen

\subsubsection{From: Nydala to Gamlia}

\textbf{All expanded nodes:}
Nydala, MIT, Berghem, Gamlia


\textbf{Path to goal:}
Nydala, MIT, Berghem, Gamlia

\subsubsection{From: Teg to Foa}

\textbf{All expanded nodes:}
Teg, Rondellen, Kyrkan, Station, Tegsbron, NUS, I20, I20, Gamlia, Gamlia, Berghem, Berghem, TreKorvar, Mariehem, Mariehem, Foa


\textbf{Path to goal:}
Teg, Rondellen, Kyrkan, NUS, Gamlia, Berghem, Mariehem, Foa

\subsection{Test Girig sökning}
\subsubsection{From: Tegsbron to Nydala}

\textbf{All expanded nodes:}
Tegsbron, I20, Station, Gamlia, Berghem, MIT, Nydala


\textbf{Path to goal:}
Tegsbron, I20, Station, Gamlia, Berghem, MIT, Nydala

\subsubsection{From: Flygplatsen to Begbilar}

\textbf{All expanded nodes:}
Flygplatsen, TreKorvar, On, Rondellen, Kyrkan, Station, Obs, Begbilar


\textbf{Path to goal:}
Flygplatsen, TreKorvar, Rondellen, Kyrkan, Station, Obs, Begbilar

\subsubsection{From: MIT to Flygplatsen}

\textbf{All expanded nodes:}
MIT, Alidhem, Sofiehem, GimonAs, NUS, Kyrkan, Rondellen, TreKorvar, Flygplatsen


\textbf{Path to goal:}
MIT, Alidhem, Sofiehem, NUS, Kyrkan, Rondellen, TreKorvar, Flygplatsen

\subsubsection{From: Nydala to Gamlia}

\textbf{All expanded nodes:}
Nydala, MIT, Berghem, Gamlia


\textbf{Path to goal:}
Nydala, MIT, Berghem, Gamlia

\subsubsection{From: Teg to Foa}

\textbf{All expanded nodes:}
Teg, Tegsbron, I20, Obs, Foa


\textbf{Path to goal:}
Teg, Tegsbron, I20, Obs, Foa

\newpage
\appendix
\section{Källkod}
\pagenumbering{roman}

\subsection{MySearcher.java}
\begin{footnotesize}
  \verbatiminput{../src/MySearcher.java}
\end{footnotesize}

\subsection{Graph.java}
\begin{footnotesize}
  \verbatiminput{../src/Graph.java}
\end{footnotesize}

\subsection{GraphNode.java}
\begin{footnotesize}
  \verbatiminput{../src/GraphNode.java}
\end{footnotesize}

\subsection{Road.java}
\begin{footnotesize}
  \verbatiminput{../src/Road.java}
\end{footnotesize}

\subsection{GraphTest.java}
\begin{footnotesize}
  \verbatiminput{../test/GraphTest.java}
\end{footnotesize}

\subsection{MySearcherLatexTest.java}
\begin{footnotesize}
  \verbatiminput{../test/MySearcherLatexTest.java}
\end{footnotesize}
\end{document}
