import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Enumeration;
import org.jdom.*;

/**
 * Denna klass implementerar olika sökalgoritmer för att hitta vägar
 * mellan två platser på en karta.
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 * @author "Victor Zamanian" <victor.zamanian@gmail.com>
 * @version 1.0
 */
public class MySearcher extends MapSearcher {

   private Graph map;
   private int topSpeed;

   /**
    * Skapar en ny MySearcher-instans.
    */
   public MySearcher () {
      super ();
      topSpeed = 0;
   }

   /**
    * Specificerar kartan utifrån en fil i XML-format. The xml-file
    * should look like:
    *<pre>
    * &lt;?xml version='1.0' encoding='ISO-8859-1' ?&gt;
    * &lt;map&gt;
    *    &lt;node id="Teg" x="1720099" y="7072732"&gt;
    *       &lt;road to="Rondellen" speed="50" /&gt;
    *       &lt;road to="Tegsbron" speed="110" /&gt;
    *    &lt;/node&gt;
    *    ...
    * &lt;/map&gt;
    *</pre>
    * @param mapFile a File containing the xml representation of the
    * map.
    */
   public void setMap(File mapFile) {
      Document doc;
      List<Element> cityElements;
      try {
         doc = loadXmlMap(mapFile);
         cityElements = doc.getRootElement().getChildren();
         map = new Graph(cityElements.size(), 5);

         // Iterate through cityElements
         for (Element cityElement: cityElements) {
            map.insertNode(cityElement.getAttributeValue("id"),
                           Integer.parseInt(cityElement.getAttributeValue("x")),
                           Integer.parseInt(cityElement.getAttributeValue("y")));
         }

         // Iterate through cityElements once more to add roads
         for (Element cityElement: cityElements) {
            GraphNode cityNode = map.getNode(cityElement.getAttributeValue("id"));

            // Iterate through roadElements in this cityElement
            List<Element> roadElements = cityElement.getChildren();
            for (Element roadElement: roadElements) {
               GraphNode nodeAtEndOfRoad = map.getNode(roadElement.getAttributeValue("to"));
               Double distance = Math.hypot((cityNode.getX() - nodeAtEndOfRoad.getX()),
                                            (cityNode.getY() - nodeAtEndOfRoad.getY()));

               Road road =
                  new Road(Integer.parseInt(roadElement.getAttributeValue("speed")),
                           distance);
               if (road.getSpeed() > topSpeed) {
                  topSpeed = road.getSpeed();
               }
               map.insertEdge(cityNode.getName(),
                              nodeAtEndOfRoad.getName(),
                              road);
            }
         }
      }
      catch (IOException e) {
         System.err.println ("Could not read/find file.");
         System.exit(1);
      }
      catch (JDOMException e) {
         System.err.println (e.getMessage());
         System.exit(1);
      }
      catch (NumberFormatException e) {
         System.err.println ("Coordinates cannot be parsed. Check XML-file for errors.");
         System.exit(1);
      }
   }

   /**
    * Utför sökning med Greedy Search.
    *
    * @param from Den plats sökningen börjar från.
    * @param to Den plats sökningen avslutas på.
    * @return En text-representation som innhåller sökvägen till
    * målet. Ex: nod1, nod2, nod3, mål
    */
   public String greedySearch (String from, String to) {
      String pathToGoal = "",
         expandedNodes = "";
      // 1. Set N to be a sorted list of initial nodes;
      PriorityQueue<GraphNode> queue =
         new PriorityQueue<GraphNode>(this.map.size());
      // Set fromNodes distance to 0.0
      map.getNode(from).setDistanceTraveled(0.0);
      queue.add(map.getNode(from));
      GraphNode n;
      while (!queue.isEmpty()) { // 2. If N is empty,
         // 3. set n to be the first node in N, and remove n from N;
         n = queue.poll();
         // store expander
         expandedNodes += n.getName() + (n == map.getNode(to) ? "" : ", ");
         n.visit();
         // 4. If n is the goal node, exit and signal success;
         if (n == this.map.getNode(to)) {
            pathToGoal = n.getName(); // Goal
            n = n.getParent();

            while (n != map.getNode(from)) {
               pathToGoal = n.getName() + ", " + pathToGoal;
               n = n.getParent();
            }
            pathToGoal = n.getName() + ", " + pathToGoal; // Beginning
            System.out.println("\nAll expanded nodes:\n" + expandedNodes + "\n\n");
            return pathToGoal;
         }
         // 5. Otherwise add the children of n to N,
         GraphNode neighbour;
         for (Enumeration<GraphNode> e = n.getNeighbours();
              e.hasMoreElements();) {
            neighbour = e.nextElement();
            neighbour.setEvalFuncVal(neighbour.getDistanceToNode(map.getNode(to)));
            if (!neighbour.isVisited() && !queue.contains(neighbour)) {
               neighbour.setParent(n);
               queue.add(neighbour);
            }
            // and return to step 2. (end of loop)
         }
      }
      // 2. ... exit and signal failure
      return "Goal not found!\n";
   }

   /**
    * Utför sökning med A*.
    *
    * @param from Den plats sökningen börjar från.
    * @param to Den plats sökningen avslutas på.
    * @param fastest Om <code>true</code>, hitta snabbaste vägen,
    * annars den kortaste.
    * @return En text-representation som innhåller sökvägen till
    * målet. Ex: nod1, nod2, nod3, mål
    */
   public String aStar (String from, String to, boolean fastest) {
      String pathToGoal = "",
         expandedNodes = "";

      // 1. Set N to be a sorted list of initial nodes;
      PriorityQueue<GraphNode> queue =
         new PriorityQueue<GraphNode>(this.map.size());
      // Set fromNodes distance to 0.0
      map.getNode(from).setDistanceTraveled(0.0);
      queue.add(map.getNode(from));

      GraphNode n;
      while (!queue.isEmpty()) { // 2. If N is empty,
         // 3. set n to be the first node in N, and remove n from N;
         n = queue.poll();
         // store expander
         expandedNodes += n.getName() + (n == map.getNode(to) ? "" : ", ");
         n.visit();
         // 4. If n is the goal node, exit and signal success;
         if (n == this.map.getNode(to)) {
            pathToGoal = n.getName(); // Goal
            n = n.getParent();

            while (n != map.getNode(from)) {
               pathToGoal = n.getName() + ", " + pathToGoal;
               n = n.getParent();
            }
            pathToGoal = n.getName() + ", " + pathToGoal; // Beginning
            System.out.println("\n    All expanded nodes:\n    " + expandedNodes + "\n\n");
            return pathToGoal;
         }

         // 5. Otherwise add the children of n to N,
         GraphNode neighbour;
         for (Enumeration<GraphNode> e = n.getNeighbours();
              e.hasMoreElements();) {
            neighbour = e.nextElement();

            if (!neighbour.isVisited()) {
               Double evalFunctionValue;
               // Fastest path
               if (fastest) {
                  evalFunctionValue = n.getDistanceTraveled()
                     + ((Road) n.getEdge(neighbour)).getTravelTime()
                     + neighbour.getDistanceToNode(map.getNode(to)) / topSpeed;
               }
               // Shortest path
               else {
                  evalFunctionValue = n.getDistanceTraveled()
                     + ((Road) n.getEdge(neighbour)).getDistance()
                     + neighbour.getDistanceToNode(map.getNode(to));
               }

               if (queue.contains(neighbour)
                   && evalFunctionValue >= neighbour.getEvalFuncVal()) {
                  // Queue already contains a better way to this
                  // node, break the for-loop here and continue.
                  continue;
               }

               if (fastest) {
                  neighbour.setDistanceTraveled(n.getDistanceTraveled()
                                                + ((Road) n.getEdge(neighbour)).getTravelTime());
               }
               else { // shortest
                  neighbour.setDistanceTraveled(n.getDistanceTraveled()
                                                + ((Road) n.getEdge(neighbour)).getDistance());
               }

               neighbour.setEvalFuncVal(evalFunctionValue);
               neighbour.setParent(n);
               
               if (!queue.contains(neighbour)) {
                  queue.add(neighbour);
               }
            }
         }
      }
      System.out.println();
      // 2. ... exit and signal failure
      return "Goal not found!\n";
   }

   /**
    * Utför bredden-förstsökning.
    *
    * @param from Den plats sökningen börjar från.
    * @param to Den plats sökningen avslutas på.
    * @return En text-representation som innhåller sökvägen till
    * målet. Ex: nod1, nod2, nod3, mål
    */
   public String breadthFirst (String from, String to) {
      /*
       * Comments fetched from
       * http://en.wikipedia.org/wiki/Breadth-first_search
       * at Tue Oct 14 12:57:18 2008
       */
      LinkedList<GraphNode> queue = new LinkedList<GraphNode>();
      String expandedNodes = "";
      String pathToGoal = "";
      // 1. Put the root node on the queue.
      queue.add(map.getNode(from));
      while (!queue.isEmpty()) {
         // 2. Pull a node from the beginning of the queue and examine it.
         GraphNode n = queue.removeFirst();
         n.visit();
         expandedNodes += n.getName() + (n == map.getNode(to) ? "" : ", ");

         //   * If the searched element is found in this node, quit
         //   * the search and return a result.
         if (n.getName().equals(to)) {
            pathToGoal = n.getName(); // Goal
            n = n.getParent();

            while (n != map.getNode(from)) {
               pathToGoal = n.getName() + ", " + pathToGoal;
               n = n.getParent();
            }
            pathToGoal = n.getName() + ", " + pathToGoal; // Beginning
            System.out.println("\nAll expanded nodes:\n" + expandedNodes + "\n\n");
            return pathToGoal;
         }
         //   * Otherwise push all the (so-far-unexamined) successors
         //   * (the direct child nodes) of this node into the end of
         //   * the queue, if there are any.
         else {
            for (Enumeration<GraphNode> e = n.getNeighbours(); e.hasMoreElements();) {
               GraphNode neighbour = e.nextElement();
               if (!neighbour.isVisited() && !queue.contains(neighbour)) {
                  neighbour.setParent(n);
                  queue.add(neighbour);
               }
            }
         }
      }
      return "Goal not found!\n";
   }

   /**
    * Utför djupet-förstsökning.
    *
    * @param from Den plats sökningen börjar från.
    * @param to Den plats sökningen avslutas på.
    * @return En text-representation som innhåller sökvägen till
    * målet. Ex: nod1, nod2, nod3, mål
    */
   public String depthFirst (String from, String to) {
      LinkedList<GraphNode> stack = new LinkedList<GraphNode>();
      String expandedNodes = "";
      String pathToGoal = "";
      // 1. Push the root node onto the stack.
      stack.add(map.getNode(from));
      while (!stack.isEmpty()) {
         // 2. Pop a node from the stack
         GraphNode n = stack.removeLast();
         //   * Mark this node as visited
         n.visit();
         //   * If the searched element is found in this node, quit
         //   * the search and return a result.
         if (n.getName().equals(to)) {
            expandedNodes += n.getName();
            pathToGoal = n.getName(); // Goal
            n = n.getParent();

            while (n != map.getNode(from)) {
               pathToGoal = n.getName() + ", " + pathToGoal;
               n = n.getParent();
            }
            pathToGoal = n.getName() + ", " + pathToGoal; // Beginning
            System.out.println("\nAll expanded nodes:\n" + expandedNodes + "\n\n");
            return pathToGoal;
         }
         //   * Otherwise push all the unvisited connecting nodes of n
         else {
            for (Enumeration<GraphNode> e = n.getNeighbours();
                 e.hasMoreElements();) {
               GraphNode neighbour = e.nextElement();
               if (!neighbour.isVisited() && !stack.contains(neighbour)) {
                  neighbour.setParent(n);
                  stack.add(neighbour);
               }
            }
         }
         expandedNodes += n.getName() + ", ";
      }
      return "Goal not found!\n";
   }

   /**
    * Returnerar en text-representation av kartan satt i metoden
    * setMap().
    *
    * @return En text-representation av kartan satt i metoden
    * setMap().
    */
   public String toString() {
      return map.toString();
   }
}